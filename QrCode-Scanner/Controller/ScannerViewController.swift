//
//  ViewController.swift
//  QrCode-Scanner
//
//  Created by SQLI on 8/2/17.
//  Copyright © 2017 SQLI. All rights reserved.
//

import UIKit
import AVFoundation

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var adminButton: UIButton!
    @IBOutlet weak var showCameraLabel: UILabel!
    @IBOutlet weak var showCameraSwitch: UISwitch!
    @IBOutlet weak var futureEventsPicker: UIPickerView!
    @IBOutlet weak var attendTypePicker: UIPickerView!
    @IBOutlet weak var responseMessage: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var countLabel: UILabel!
    var futureEvents: [[String: String]] = [["id": "1", "name": "Velg Arrangement"]]
    var attendType: [String] = ["Vanlig innslipp", "Matinnslipp", "Åpne for alle"]
    var userName: String! = ""
    var fullName: String! = ""
    var selectedEventID: String! = ""
    var selectedAttendingType: String! = ""
    var currentCount: Int = 0
    var maxCount: Int = 0
    var status: Int = 1
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    var studentStatusChanged: Bool = false
    var didLayOutSubivews: Bool = false
    var captureSession:AVCaptureSession? //captureSession manages capture activity and coordinates between input device and captures outputs
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    
    //Empty Rectangle with green border to outline detected QR or BarCode
    let codeFrame:UIView = {
        let codeFrame = UIView()
        codeFrame.layer.borderColor = UIColor.green.cgColor
        codeFrame.layer.borderWidth = 1.5
        codeFrame.frame = CGRect.zero
        codeFrame.translatesAutoresizingMaskIntoConstraints = false
        return codeFrame
    }()
    
    override func viewDidDisappear(_ animated: Bool) {
        captureSession?.stopRunning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        captureSession?.startRunning()
    }
    
    @IBAction func adminButtonPressed(_ sender: Any) {
        self.futureEventsPicker.reloadAllComponents()
        self.futureEventsPicker.isHidden = !self.futureEventsPicker.isHidden
        self.attendTypePicker.isHidden = !self.attendTypePicker.isHidden
        self.showCameraLabel.isHidden = !self.showCameraLabel.isHidden
        self.showCameraSwitch.isHidden = !self.showCameraSwitch.isHidden
        view.bringSubviewToFront(showCameraSwitch)
        if (self.showCameraLabel.isHidden == false){
            self.videoPreviewLayer!.isHidden = true
        }
        else{
            self.videoPreviewLayer!.isHidden = !self.showCameraSwitch.isOn
            codeFrame.isHidden = !self.showCameraSwitch.isOn
        }
    }
    
    @IBAction func showCameraToggle(_ sender: Any) {

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getFutureEvents()
        responseMessage.layer.masksToBounds = true
        responseMessage.layer.cornerRadius = 15
        self.futureEventsPicker.dataSource = self
        self.futureEventsPicker.delegate = self
        self.attendTypePicker.dataSource = self
        self.attendTypePicker.delegate = self
        self.userNameTextField.delegate = self
        userNameTextField.returnKeyType = .go
        
        //AVCaptureDevice allows us to reference a physical capture device (front camera)
        let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .front)

        if let captureDevice = captureDevice {

            do {
                
                captureSession = AVCaptureSession()
                
                // CaptureSession needs an input to capture Data from
                let input = try AVCaptureDeviceInput(device: captureDevice)
                captureSession?.addInput(input)
                
                // CaptureSession needs and output to transfer Data to
                let captureMetadataOutput = AVCaptureMetadataOutput()
                captureSession?.addOutput(captureMetadataOutput)

                //We tell our Output the expected Meta-data type
                captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                captureMetadataOutput.metadataObjectTypes = [.code128, .qr,.ean13, .ean8, .code39, .upce, .aztec, .pdf417] //AVMetadataObject.ObjectType
                
                captureSession?.startRunning()

                //The videoPreviewLayer displays video in conjunction with the captureSession
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                videoPreviewLayer?.videoGravity = .resizeAspectFill
                //videoPreviewLayer?.connection?.videoOrientation = UIDevice.current.orientation as AVCaptureVideoOrientation
                screenWidth = view.layer.bounds.width
                screenHeight = view.layer.bounds.height
                videoPreviewLayer?.frame = CGRect(x: screenWidth * 0.25, y: screenHeight * 0.6, width: screenWidth * 0.5, height: screenHeight * 0.4)
                view.layer.addSublayer(videoPreviewLayer!)
                
                view.bringSubviewToFront(futureEventsPicker)
                view.bringSubviewToFront(attendTypePicker)
            
            }
            catch {
                print("Error with capture session")
            }
        }
        
    }
    
    func setCameraOrientation(){
        switch (UIDevice.current.orientation){
        case .landscapeLeft:
            print("left")
            videoPreviewLayer?.connection?.videoOrientation = .landscapeRight
        case .landscapeRight:
            print("right")
            videoPreviewLayer?.connection?.videoOrientation = .landscapeLeft
        default:
            print("default")
            videoPreviewLayer?.connection?.videoOrientation = .landscapeLeft
        }
    }
    override func viewDidLayoutSubviews() {
        if didLayOutSubivews == false{
            setCameraOrientation()
            didLayOutSubivews = true
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        setCameraOrientation()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        userName = userNameTextField.text
        userNameTextField.text = ""
        userNameTextField.resignFirstResponder()
        checkStudentAttendingStatus()
        studentStatusChanged = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.textFromStatus()
        })
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == futureEventsPicker {
            return futureEvents.count
        } else {
            return attendType.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == futureEventsPicker {
            return futureEvents[row]["name"] as String?
        } else {
            return attendType[row] as String?
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == futureEventsPicker {
            selectedEventID = (futureEvents[row])["id"]!
        } else {
            switch attendType[row]{
            case "Vanlig innslipp":
                selectedAttendingType = ""
            case "Matinnslipp":
                selectedAttendingType = "food"
            case "Åpne for alle":
                selectedAttendingType = "all"
            default:
                selectedAttendingType = ""
            }
        }
    }
    
    // the metadataOutput function informs our delegate (the ScannerViewController) that the captureOutput emitted a new metaData Object
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            print("no objects returned")
            return
        }
        
        let metaDataObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        guard let StringCodeValue = metaDataObject.stringValue else {
            return
        }
        
        view.addSubview(codeFrame)
        
        //transformedMetaDataObject returns layer coordinates/height/width from visual properties
        guard let metaDataCoordinates = videoPreviewLayer?.transformedMetadataObject(for: metaDataObject) else {
            return
        }
        
        //Those coordinates are assigned to our codeFrame
        codeFrame.frame = metaDataCoordinates.bounds
        codeFrame.center.x += view.layer.bounds.width * 0.25
        codeFrame.center.y += view.layer.bounds.height * 0.6

        if (userName != StringCodeValue || studentStatusChanged){
            print(StringCodeValue)
            userName = StringCodeValue.replacingOccurrences(of: "@stud.ntnu.no", with: "")
            checkStudentAttendingStatus()
            studentStatusChanged = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.textFromStatus()
            })
        }
        
    }
    
    func getFutureEvents() {
        // Set up the URL request
        let todoEndpoint: String = "https://bindeleddet.no/api/v1/attendee_registration"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let content = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        print("error trying to convert data to JSON")
                        return
                }
                // now we have the todo
                // let's just print it to prove we can access it
                //print(content.description)
                
                // the todo object is a dictionary
                // so we just access the title using the "title" key
                // so check for a title and print it if we have one
                guard let futureEvents = content["future_events"] as? NSArray else {
                    print("Could not get todo title from JSON")
                    return
                }
                for item in futureEvents{
                    guard let value = item as? NSDictionary else{
                        print("Could not convert event to NSDictionary")
                        return
                    }
                    guard let id = value["id"] as? Int else{
                        print("Could not get id from NSDictionary")
                        return
                    }
                    guard let name = value["name"] as? String else{
                        print("Could not get name from NSDictionary")
                        return
                    }
                    self.futureEvents.append(["id": String(id), "name": name])
                }
                self.futureEvents.remove(at: 0)
                self.selectedEventID = (self.futureEvents[0])["id"]
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
    }
    
    func checkStudentAttendingStatus() {
        var todosEndpoint: String = "https://bindeleddet.no/api/v1/attendee_registration/" + selectedEventID + "/register_student?user_input="
        todosEndpoint += userName + "&open_status=" + selectedAttendingType
        guard let todosURL = URL(string: todosEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        var todosUrlRequest = URLRequest(url: todosURL)
        todosUrlRequest.httpMethod = "POST"
        todosUrlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let newTodo: [String: String] = ["secret_key": "hasayc7r38qccqm7oq8xn7q2cn89asdn84o8qxn4m81z2"]
        let jsonTodo: Data
        do {
            jsonTodo = try JSONSerialization.data(withJSONObject: newTodo, options: [])
            todosUrlRequest.httpBody = jsonTodo
        } catch {
            print("Error: cannot create JSON from todo")
            return
        }
        print(todosURL)
        let session = URLSession.shared
        
        let task = session.dataTask(with: todosUrlRequest) {
            (data, response, error) in
            guard error == nil else {
                print("error calling POST on /todos/1")
                print(error!)
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let receivedJSON = try JSONSerialization.jsonObject(with: responseData,
                    options: []) as? [String: Any] else {
                    print("Could not get JSON from responseData as dictionary")
                    return
                }
                print("The todo is: " + receivedJSON.description)
                guard let registration = receivedJSON["registration"] as? NSDictionary else {
                    print("Could not get todoID as int from JSON")
                    return
                }
                guard let status = registration["status"] as? Int else {
                    print("Could not get todoID as int from JSON")
                    return
                }
                if (self.status != status){
                    self.studentStatusChanged = true
                }
                self.status = status
                guard let count = receivedJSON["count"] as? NSDictionary else {
                    print("Could not get todoID as int from JSON")
                    return
                }
                guard let current = count["current"] as? Int else {
                    print("Could not get todoID as int from JSON")
                    return
                }
                guard let max = count["max"] as? Int else {
                    print("Could not get todoID as int from JSON")
                    return
                }
                
                guard let student = registration["student"] as? NSDictionary else {
                    print("Could not get todoID as int from JSON")
                    return
                }
                guard let full_name = student["full_name"] as? String else {
                    print("Could not get todoID as int from JSON")
                    return
                }
                
                self.currentCount = current
                self.maxCount = max
                self.fullName = full_name
            } catch  {
                print("error parsing response from POST on /todos")
                return
            }
        }
        task.resume()
    }
    
    func textFromStatus(){
        switch self.status {
        case 1:
            responseMessage.text = "Velkommen, " + fullName
            responseMessage.backgroundColor = UIColor.green
        case 2:
            responseMessage.text = "Det skjedde noe galt, prøv igjen!"
            responseMessage.backgroundColor = UIColor.orange
        case 3:
            responseMessage.text = "Du har allerede møtt opp, " + fullName
            responseMessage.backgroundColor = UIColor.orange
        case 4:
            responseMessage.text = "Du er ikke påmeldt denne bedriftspresentasjonen"
            responseMessage.backgroundColor = UIColor.red
        case 5:
            responseMessage.text = "Finner ikke studenten"
            responseMessage.backgroundColor = UIColor.red
        case 6:
            responseMessage.text = "Du har allerede møtt opp til bespisningen, " + fullName
            responseMessage.backgroundColor = UIColor.orange
        case 7:
            responseMessage.text = "Velkommen til bespisningen, " + fullName
            responseMessage.backgroundColor = UIColor.green
        case 8:
            responseMessage.text = "Du var ikke på presentasjonen og kan derfor ikke delta på bespisningen"
            responseMessage.backgroundColor = UIColor.red
        default:
            responseMessage.text = "En feil med oppmøtesystemet skjedde"
            responseMessage.backgroundColor = UIColor.orange
        }
        countLabel.text = String(currentCount) + "/" + String(maxCount)
        UIView.animate(withDuration: 0.5,
            animations: {
                self.responseMessage.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        },
            completion: { _ in
            UIView.animate(withDuration: 0.5) {
                self.responseMessage.transform = CGAffineTransform.identity
            }
        })
    }
    
}


